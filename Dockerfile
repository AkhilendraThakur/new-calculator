FROM python:3.11.2
WORKDIR /projects/pythonproject2
COPY . /projects/pythonproject2
ENV PYTHONUNBUFFERED=1
CMD ["python3", "Simple-Calculator.py"]
