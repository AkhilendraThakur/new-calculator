import sys

def add(x, y):
    return x + y

def subtract(x, y):
    return x - y

def multiply(x, y):
    return x * y

def divide(x, y):
    if y == 0:
        print("Error! Division by zero.")
        sys.exit(1)
    return x / y

def main():

    num1 = 2
    num2 = 3
    
    print(f"The addition is: {add(num1, num2)}")
    print(f"The subtraction is: {subtract(num1, num2)}")
    print(f"The multiplication is: {multiply(num1, num2)}")
    print(f"The division is: {divide(num1, num2):.2f}")

if __name__ == "__main__":
    main()